package br.com.itau.cliente.controller;

import br.com.itau.cliente.dto.ClienteRequest;
import br.com.itau.cliente.dto.ClienteResponse;
import br.com.itau.cliente.mapper.ClienteMapper;
import br.com.itau.cliente.model.Cliente;
import br.com.itau.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @GetMapping("/{id}")
    public ClienteResponse buscar(@PathVariable Integer id) {
        Cliente cliente = clienteService.buscarPorId(id);
        System.out.println("Buscando cliente: " + id);
        return clienteMapper.toClienteResponse(cliente);
    }

    @GetMapping
    public List<ClienteResponse> listarClientes() {
        Iterable<Cliente> clientes = clienteService.listarClientes();
        return clienteMapper.toClienteResponseList(clientes);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ClienteResponse criarUsuario(@Valid @RequestBody ClienteRequest clienteRequestDto) {
        Cliente cliente = clienteMapper.toCliente(clienteRequestDto);
        cliente = clienteService.criarCliente(cliente);
        System.out.println("Criado cliente - " + cliente.getId() + " - " + cliente.getName());
        return clienteMapper.toClienteResponse(cliente);
    }
}

