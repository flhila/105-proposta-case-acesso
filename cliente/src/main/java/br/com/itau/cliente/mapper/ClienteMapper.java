package br.com.itau.cliente.mapper;

import br.com.itau.cliente.dto.ClienteRequest;
import br.com.itau.cliente.dto.ClienteResponse;
import br.com.itau.cliente.model.Cliente;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClienteMapper {

    public Cliente toCliente(ClienteRequest clienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

    public ClienteResponse toClienteResponse(Cliente cliente) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setName(cliente.getName());
        return clienteResponse;
    }

    public List<ClienteResponse> toClienteResponseList(Iterable<Cliente> clientes) {
        List<ClienteResponse> clienteResponses = new ArrayList<ClienteResponse>();
        for (Cliente cliente: clientes) {
            clienteResponses.add(toClienteResponse(cliente));
        }
        return clienteResponses;
    }
}
