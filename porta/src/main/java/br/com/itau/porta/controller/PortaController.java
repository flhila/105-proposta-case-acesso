package br.com.itau.porta.controller;

import br.com.itau.porta.dto.PortaRequest;
import br.com.itau.porta.dto.PortaResponse;
import br.com.itau.porta.mapper.PortaMapper;
import br.com.itau.porta.model.Porta;
import br.com.itau.porta.service.PortaService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @Autowired
    private PortaMapper portaMapper;

    @GetMapping("/{id}")
    public PortaResponse buscar(@PathVariable Long id) {
        Porta porta = portaService.buscarPorId(id);
        System.out.println("Buscando porta: " + id);
        return portaMapper.toPortaResponse(porta);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public PortaResponse criarPorta(@Valid @RequestBody PortaRequest portaRequestDto) {
        Porta porta = portaMapper.toPorta(portaRequestDto);
        porta = portaService.criarPorta(porta);
        System.out.println("Criada porta - " + porta.getId());
        return portaMapper.toPortaResponse(porta);
    }
}
