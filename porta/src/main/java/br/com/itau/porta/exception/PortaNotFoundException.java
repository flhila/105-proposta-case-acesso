package br.com.itau.porta.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta não encontrada com o id informado")
public class PortaNotFoundException extends RuntimeException {
}
