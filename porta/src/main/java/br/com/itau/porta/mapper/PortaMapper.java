package br.com.itau.porta.mapper;

import br.com.itau.porta.dto.PortaRequest;
import br.com.itau.porta.dto.PortaResponse;
import br.com.itau.porta.model.Porta;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {

    public PortaResponse toPortaResponse(Porta porta) {
        PortaResponse portaResponse = new PortaResponse();
        portaResponse.setAndar(porta.getAndar());
        portaResponse.setId(porta.getId());
        portaResponse.setSala(porta.getSala());
        return portaResponse;
    }

    public Porta toPorta(PortaRequest portaRequestDto) {
        Porta porta = new Porta();
        porta.setAndar(portaRequestDto.getAndar());
        porta.setSala(portaRequestDto.getSala());
        return porta;
    }
}
