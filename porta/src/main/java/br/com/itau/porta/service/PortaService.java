package br.com.itau.porta.service;

import br.com.itau.porta.exception.PortaNotFoundException;
import br.com.itau.porta.model.Porta;
import br.com.itau.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta buscarPorId(Long id) {
        Optional<Porta> porta = portaRepository.findById(id);
        if (porta.isPresent()) {
            return porta.get();
        } else {
            throw  new PortaNotFoundException();
        }
    }

    public Porta criarPorta(Porta porta) {
        return portaRepository.save(porta);
    }
}
